// eslint-disable-next-line import/no-extraneous-dependencies
import {useEffect, useState} from "preact/hooks";
import axios, {AxiosRequestConfig} from "axios";
import qs from "qs";
import Web3Modal from "web3modal";
import BurnerConnectProvider from "@burner-wallet/burner-connect-provider";
import * as tus from "tus-js-client";
import {
  API_RANKING,
  API_FEEDBACK,
  API_BADGES,
  API_FEEDBACK_SUMMARY,
  CORE_API_BASE,
  UPLOADER_API_BASE,
  VIDEO_PUBLISHED_BASE
} from "../names";
import {useAuth} from "./auth";

export interface IEntity {
  name: string;
  uuid: string;
  image: string;
  type: string;
  properties: Record<string, unknown>;
}

export interface IRankingItem {
  entity: IEntity;
  relationshipPaths: []; // ToDo: define type
  summaryText: string;
  summaryImages: string[];
}

export interface IRankingsResponse {
  status: "success" | "error"; // ToDo: verify status enum values
  result: IRankingItem[];
}

/** possible badge qualifiers */
type BadgeQualifier = "positive" | "negative" | "neutral";

/** used for assigning badges as part of feedback */
export interface IBadges {
  [id: string]: BadgeQualifier;
}

/** for providing feedback */
export interface IFeedbackData {

  badges?: IBadges;
  review?: string;
  video?: string;
  stars?: number;
  endorsements?: IEndorsements[];
}

export interface IBadgeSet {
  id: string;
  image: string;
  badges: {
    qualifier: BadgeQualifier;
    data: {
      image: string;
      label: string;
    };
  }[];
}

export interface IFeedbackResponse {
  status: "success" | "error"; // ToDo: verify status enum values
  result: IFeedbackItem;
}

export interface IFeedbackItem {
  items: IFeedback;
  targetCriteria: IFeedbackTarget;
}

export interface IFeedbackTarget {
  ids: [id
        :
        string
  ]
}

export interface IFeedback {
  summaryText: string;
  contacts?: IContacts;
  badges: IBadgesFb;
  reviews: IReviews[];
  stars: IStars;
  videos: IVideo;
  endorsements?: IEndorsements[];
}

export interface IBadgesFb {
  [id: string]: IFb;
}

export interface IFb {
  [qualifier: string]: IBadgeCount;
}

export interface IEndorsements {
  source: IEntity;
  endorsement: IEndorsement;
}

export interface IEndorsement {
  value: number;
  blockNumber: number;
}

export interface IProperties {
  value: number;
}

export interface IBadgeCount {
  badge: IBadge;
  count: number;
}

export interface IBadge {
  profile: [];
  imageBig: string;
  image: string;
  label: string;
}

export interface IReviews {
  content: string;
  date: number;
  image: string;
  summary: string;
}

export interface IContacts {
  contact1: string;
  contact2: string;
  contact3: string;
}

export interface IStars {
  avg: number;
  count: number;
  sum: number;
  name: [];
  summaryText: string;
}

export interface IVideo {
  summaryText: string;
  videos: IVideos[];
}

export interface IVideos {
  date: number;
  image: string;
  url: string;
}


export interface IBadgeAssignment {
  id: string,
  value: BadgeQualifier
}

export function useWalletConnectApi() {
  const [web3Modal, setWeb3Modal] = useState(null);

  useEffect(() => {
    async function f() {

      const providerOptions = {
        burnerconnect: {
          package: BurnerConnectProvider, // required
          options: {defaultNetwork: "100"}
        }
      };

      const newWeb3Modal = new Web3Modal({
        cacheProvider: true, // very important
        network: "mainnet",
        providerOptions
      });

      setWeb3Modal(newWeb3Modal);
    }

    f()
  }, [setWeb3Modal])
  return {web3Modal};
}


export function useRankingApi(apiUrl: string, sourceUuid: string, targetType: string) {

  const {accessToken} = useAuth();
  const [rankingItems, setRankingItems] = useState<IRankingItem[]>(undefined);

  const queryParams = qs.stringify({
    sourceCriteria: JSON.stringify(createEntityCriteria(sourceUuid)),
    targetType
  });

  useEffect(() => {
    async function f() {
      // Passing queryParams as config.params doesn't set them correctly, so we just append them to the URL:
      const response = await axios.get<IRankingsResponse>(`${apiUrl}${CORE_API_BASE}${API_RANKING}?${queryParams}`,
        withAuthorizationHeader(accessToken));
      setRankingItems(response.data.result);
    }

    f();
  }, [setRankingItems, apiUrl, queryParams, accessToken]);

  return {rankingItems};
}


export enum SubmitStatus {
  idle,
  submitting,
  success,
  error
}

const SUBMIT_STATUS_RESET_DELAY_MS = 3000;

/**
 * Calls the given submitStatusSetter with SubmitStatus.idle after a delay of SUBMIT_STATUS_RESET_DELAY_MS
 * @param submitStatusSetter
 */
function resetSubmitStatusDelayed(submitStatusSetter: (submitStatus: SubmitStatus) => void) {
  setTimeout(() => submitStatusSetter(SubmitStatus.idle), SUBMIT_STATUS_RESET_DELAY_MS);
}

/**
 * Use the UTU API to post feedback.
 * @param apiUrl
 * @param sourceUuid
 * @param targetUuid
 * @param transactionId
 * @return a function which takes an IFeedbackData parameter and returns a promise which will resolve with an
 *         unspecified value when the feedback was posted successfully, and will reject otherwise.
 */
export function useFeedbackApi(apiUrl: string, sourceUuid: string, targetUuid: string, transactionId: string)
  : { sendFeedback: (feedbackData: IFeedbackData) => Promise<any>, submitStatus: SubmitStatus } {
  const {accessToken} = useAuth();
  const [submitStatus, setSubmitStatus] = useState(SubmitStatus.idle);

  const sourceCriteria = createEntityCriteria(sourceUuid);
  const targetCriteria = createEntityCriteria(targetUuid);

  function sendFeedback(feedbackData: IFeedbackData): Promise<any> {
    setSubmitStatus(SubmitStatus.submitting);
    return axios.post(
      `${apiUrl}${CORE_API_BASE}${API_FEEDBACK}`,
      {
        sourceCriteria,
        targetCriteria,
        transactionId,
        items: feedbackData
      },
      withAuthorizationHeader(accessToken)
    ).then(result => {
      setSubmitStatus(SubmitStatus.success);
      resetSubmitStatusDelayed(setSubmitStatus);
      return result;
    }).catch(error => {
      setSubmitStatus(SubmitStatus.error);
      resetSubmitStatusDelayed(setSubmitStatus);
      return error;
    });
  }

  return {sendFeedback, submitStatus};
}

export function useBadgesApi(apiUrl: string) {
  const {accessToken} = useAuth();
  const [badgeSets, setBadgeSets] = useState<IBadgeSet[]>([]);

  useEffect(() => {
    async function f() {
      const response = await axios.get(`${apiUrl}${CORE_API_BASE}${API_BADGES}`, withAuthorizationHeader(accessToken));
      setBadgeSets(response.data.result);
    }

    f();
  }, [setBadgeSets, apiUrl, accessToken]);

  return {badgeSets};
}

export function useFeedbackSummaryApi(apiUrl: string, sourceUuid: string, targetUuid: string)
  : { feedbackSummary: IFeedback | undefined, submitStatus: SubmitStatus } {
  const {accessToken} = useAuth();
  const [feedbackSummary, setFeedbackSummary] = useState<IFeedback>(undefined);
  const [submitStatus, setSubmitStatus] = useState(SubmitStatus.idle);

  const sourceCriteria = createEntityCriteria(sourceUuid);
  const targetCriteria = createEntityCriteria(targetUuid);

  const queryParams = qs.stringify({
    // sourceCriteria, don't send for now, there's a bug in the API which then only returns feedback from that
    // source, but not from their connections
    sourceCriteria, targetCriteria
  });

  useEffect(() => {
    async function f() {
      setSubmitStatus(SubmitStatus.submitting);
      await axios.get<IFeedbackResponse>(`${apiUrl}${CORE_API_BASE}${API_FEEDBACK_SUMMARY}?${queryParams}`,
        withAuthorizationHeader(accessToken)).then(result => {
        setSubmitStatus(SubmitStatus.success);
        setFeedbackSummary(result.data.result.items);
      }).catch(error => {
        setSubmitStatus(SubmitStatus.error);
        return error;
      });

    }

    f();
  }, [setFeedbackSummary, apiUrl, queryParams, accessToken]);

  return {feedbackSummary, submitStatus};
}


export function useUploadVideoApi(apiUrl: string, source?: File | Blob | Pick<ReadableStreamDefaultReader, "read">) {
  const {accessToken} = useAuth();

  const [status, setStatus] = useState<{
    successMessage?: string;
    errorMessage?: string;
    uploading?: boolean;
    error?: Error;
    publishedVideoUrl?: string;
  }>({
    uploading: false
  });

  useEffect(() => {
    if (!source) return;

    const wrappedReader = {
      read() {
        const result = (source as ReadableStreamDefaultReader).read();
        return result;
      }
    };

    const options = {
      resume: false,
      endpoint: process.env.UTU_VIDEO_UPLOAD_URL || `${apiUrl}${UPLOADER_API_BASE}/upload`,
      headers: getAuthorizationHeader(accessToken),
      chunkSize: 28 * 1024,
      uploadLengthDeferred: true,
      retryDelays: [0, 3000, 5000, 10000, 20000],
      metadata: {
        filename: "webcam.webm",
        filetype: "video/webm"
      },
      onError() {
        setStatus({uploading: false, errorMessage: "video was not successfully uploaded "});
      },
      onSuccess() {
        const publishedVideoUrl = getPublishedVideoUrl(apiUrl, upload.url);
        const checkVideoInterval = setInterval(() => {
          axios.head(publishedVideoUrl).then(() => {
            setStatus({uploading: false, publishedVideoUrl});
            clearInterval(checkVideoInterval);
          }).catch(
            () => setStatus({errorMessage: "Video not yet available."}))
        }, 500);

        setStatus({successMessage: "video was successfully uploaded"});
      }
    };

    const upload = new tus.Upload(wrappedReader, options);


    // Start the upload
    upload.start();
    setStatus({uploading: true});

  }, [accessToken, source]);

  return status;
}

function getPublishedVideoUrl(apiUrl: string, uploadUrl: string) {
  const videoId = uploadUrl.split("/").pop();
  const publishedUrl = process.env.UTU_VIDEO_PUBLISHED_BASE_URL
    ? `${process.env.UTU_VIDEO_PUBLISHED_BASE_URL}/${videoId}`
    : `${apiUrl}${VIDEO_PUBLISHED_BASE}/${videoId}`;
  return publishedUrl;
}

function createEntityCriteria(uuid: string) {
  return {ids: {uuid}};
}

function withAuthorizationHeader(accessToken: string, config: AxiosRequestConfig = {}) {
  return {
    ...config,
    headers: {
      ...(config.headers || {}),
      ...getAuthorizationHeader(accessToken)
    }
  };
}

function getAuthorizationHeader(accessToken: string) {
  return {"Authorization": `Bearer ${accessToken}`};
}
