/* eslint-disable camelcase */
import { useEffect, useState } from "preact/hooks";
import jwtDecode from "jwt-decode";
import { ethers } from "ethers";
import {
  EVENT_UTU_IDENTITY_DATA_READY,
  LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA,
  UTU_API_BASE_URL_PRODUCTION,
  UTU_API_BASE_URL_TEST,
  API_VERIFY_ADDRESS,
} from "../names";
import axios from "axios";

import {config, config as SDKConfig} from "../components/BaseComponent/BaseComponent";
import { getProvider } from "../components/EndorsementForm/networks";

export type AuthData = {
  access_token: string;
  expires_in: number;
  refresh_expires_in: number;
  refresh_token: string;
  token_type: string;
  session_state: string;
  scope: string;
};

export function useAuth() {
  const [accessToken, setAccessToken] = useState(
    getAccessTokenFromLocalStorage()
  );

  useEffect(() => {
    function onUtuTokensReady(event: CustomEvent) {
      const { access_token } = event.detail;
      setAccessToken(access_token);
    }

    window.addEventListener(EVENT_UTU_IDENTITY_DATA_READY, onUtuTokensReady);

    return () => {
      window.removeEventListener(
        EVENT_UTU_IDENTITY_DATA_READY,
        onUtuTokensReady
      );
    };
  }, []);

  return { accessToken };
}

export async function addressSignatureVerification(
  overrideApiUrl: string | null = null,
  walletProvider: any = null,
  cookies = false
): Promise<AuthData | null> {

  const utuApiBase = overrideApiUrl ?? (config.production ? UTU_API_BASE_URL_PRODUCTION : UTU_API_BASE_URL_TEST);
  const _provider = getProvider();
  // @ts-ignore
  if (!_provider && !walletProvider) return null;
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(walletProvider || _provider);

  const signer = provider.getSigner();

  const address = await signer.getAddress();
  const signature = await signer.signMessage("Sign in at UTU");

  const { data } = await axios.post(
    `${utuApiBase}${API_VERIFY_ADDRESS}`,
    {
      address,
      signature,
    },
    {
      withCredentials: !!cookies,
    }
  );

  await localStorage.setItem(
    LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA,
    JSON.stringify(data)
  );

  data as AuthData;

  return data;
}

function getAccessTokenFromLocalStorage() {
  const item = localStorage.getItem(LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA);

  if (item) {
    const { access_token } = JSON.parse(item);
    const { exp } = jwtDecode(access_token) as { exp: number };

    if (isValid(exp)) {
      return access_token;
    }
  }

  return null;
}

function isValid(exp: number) {
  return Date.now() < exp * 1000;
}
