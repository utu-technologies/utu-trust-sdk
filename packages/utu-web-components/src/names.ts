// Names that are part of the external SDK interface, i.e. those which our clients use.

// Names of all provided x-tags.
export const TAG_HEADER = "x-utu-header";
export const TAG_ROOT = "x-utu-root";
export const TAG_UTU_APP_LINK= "x-utu-app-link";
export const TAG_RECOMMENDATION = "x-utu-recommendation";
export const TAG_FEEDBACK_FORM = "x-utu-feedback-form";
export const TAG_FEEDBACK_FORM_POPUP = "x-utu-feedback-form-popup";
export const TAG_FEEDBACK_DETAILS_POPUP = "x-utu-feedback-details-popup";
export const TAG_FEEDBACK_DETAILS = "x-utu-feedback-details";
export const TAG_TOGGLE_BUTTON = "x-utu-toggle-button";

export const TAG_WALLET_CONNECT = "x-utu-wallet-connect";
export const TAG_WALLET_DISCONNECT = "x-utu-wallet-disconnect";
export const TAG_UTT_BALANCE = "x-utt-balance";

// Attribute names of all attributes provided by all TAGs.
export const ATTR_API_URL = "api-url";

export const ATTR_BTN_COLOR = "btn-color";
export const ATTR_THEME_COLOR = "theme-color";

export const ATTR_SOURCE_UUID = "source-uuid";

export const ATTR_TARGET_TYPE = "target-type";
export const ATTR_TARGET_UUIDS = "target-uuids";
export const ATTR_TARGET_UUID = "target-uuid";
export const ATTR_ENDORSEMENT_NETWORK = "endorsement-network";

export const ATTR_TRANSACTION_ID = "transaction-id";

// API BASE URLs
export const UTU_API_BASE_URL_PRODUCTION = "https://api.ututrust.com";
export const UTU_API_BASE_URL_TEST = "https://stage-api.ututrust.com";

export const UTU_SOCIAL_LINK_URL_PRODUCTION ="https://app.utu.io";
export const UTU_SOCIAL_LINK_URL_TEST = "https://stage-app.utu.io";

export const CORE_API_BASE = "/core-api-v2";

export const UPLOADER_API_BASE = "/uploader-api";
export const VIDEO_PUBLISHED_BASE = "/published-videos";

// API resource paths
export const API_RANKING = "/ranking";
export const API_FEEDBACK = "/feedback";
export const API_BADGES = "/badges";
export const API_FEEDBACK_SUMMARY = "/feedbackSummary";
export const API_VERIFY_ADDRESS = "/identity-api/verify-address";

// Events
export const EVENT_UTU_CONFIG = "utuConfig";
export const EVENT_UTU_IDENTITY_DATA_READY = "utuIdentityDataReady";

// Local Storage Keys
export const LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA = "utu-identity-data";

// Supported UTT networks
export enum NETWORK_NAME {
  polygon_mainnet = "polygon_mainnet",
  polygon_mumbai = "polygon_mumbai"
};
