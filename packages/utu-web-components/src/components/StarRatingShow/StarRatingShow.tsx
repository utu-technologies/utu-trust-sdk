
/* eslint-disable no-nested-ternary */
import { Fragment, h } from "preact";
import { useState } from "preact/hooks";
import ReactStars from "react-stars";
import { BaseComponent } from "../BaseComponent";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import {
  ATTR_THEME_COLOR
} from "../../names";
import style from "./StarRatingShow.scss";
import FeedbackDetailsStatusView from "../SubmitStatusView/FeedbackDetailsStatusView";
import Avatar from "../FeedbackDetails/Avatar";


export default function StarRatingShow(props: IFeedbackDetailsProps) {
 
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"
    
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  if (!props.feedbackSummary) {
    return  (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div/></FeedbackDetailsStatusView>);
  }

  // if (!props.feedbackSummary) {
  //   return <Fragment />;
  // }

  if (!props.feedbackSummary.stars) {
    return <Fragment />;
  }

  // star rating
  // const stars = (typeof  props.feedbackSummary.stars.avg === "undefined") ? 0 :  props.feedbackSummary.stars.avg;
  
  const stars = props.feedbackSummary?.stars?.avg;
  const {summaryText} = props.feedbackSummary.stars;


  return (
    <BaseComponent style={style}>
      {stars > 0 ?
        <FeedbackDetailsStatusView submitStatus={props.submitStatus}>
       
          <section className={`stars-section x-utu-section-${envCondition}`}>
            <div className="stars-section-align">
              <ReactStars
                count={5}
                value={stars}
                size={30}
                color2="#FFDD33"
                edit={false}
                className="pr-4"
              />
            </div>
            <div className="stars-section-text"><p className="stars-section-text-content">{summaryText}</p></div>
          </section>
        </FeedbackDetailsStatusView> : null
      }
     
    </BaseComponent>

  );
}
