/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useContext, useState } from "preact/hooks";
import ReactStars from "react-stars";

import {BaseComponent, getBaseProps} from "../BaseComponent";
import { IFeedbackProps } from "../FeedbackForm/FeedbackProps";
import { useFeedbackApi } from "../../hooks/api";
import {
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID,
  TAG_FEEDBACK_FORM
} from "../../names";

import style from "./StarRatingInput.scss";
import SubmitStatusView from "../SubmitStatusView/SubmitStatusView";
import { WalletAddressContext } from "../WalletConnect/WalletAddressContext";


export default function StarRatingInput(props: IFeedbackProps) {
  const [starRatings, setstarRatings] = useState(1);
  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_FORM);

  const walletAddress = useContext(WalletAddressContext);
  if (props[ATTR_SOURCE_UUID] === "address") {
    props[ATTR_SOURCE_UUID] = walletAddress;
  }

  const { sendFeedback, submitStatus } = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID],
    props[ATTR_TRANSACTION_ID]);

  // environments
  const [isDark, setIsDark] = useState(false);

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }


  const ratingChanged = (newRating: number) => {
    setstarRatings(newRating);
    sendFeedback({ stars: newRating });
  };

  return (
    <BaseComponent 
      style={style} 
      className={`star-rating-section x-utu-section x-utu-section-${
        isDark ? "dark" : "light"
      }`} excludeBootstrap excludeFonts>
      <SubmitStatusView submitStatus={submitStatus}>
        <ReactStars
          count={5}
          half={false}
          onChange={ratingChanged}
          size={30}
          color2="#FFDD33"
          // color2= {ratingColor}
        />
      </SubmitStatusView>
      <p className="mt-3 mx-2 utu-star-rating-text">{starRatings} / 5.0</p>
    </BaseComponent>
  );
}
