import { ethers, providers } from "ethers";
import { h } from "preact";
import { useEffect, useState } from "preact/hooks";
import { abi as UTTAbi } from "../../contracts/UTT.abi.json";
import { BaseComponent } from "../BaseComponent";
import { getDefaultNetworkName, getProvider, getUTTContractAddress, requestNetworkChange } from "../EndorsementForm/networks";
import style from "./UttBalance.scss";

export default function UttBalance() {
  const [UTTWalletState, setUTTWalletState] = useState(0);

  const fetchBalance = async () => {
    try {
      const provider = await getProvider();
      const network = getDefaultNetworkName();
      const contractAddress = getUTTContractAddress(network);
      await requestNetworkChange(provider, network);
      const ethersProvider = new providers.Web3Provider(provider);
      const userAddress = await ethersProvider.getSigner().getAddress();
      const getContract = new ethers.Contract(contractAddress, UTTAbi, ethersProvider);
      const walletUTTBalance = await getContract.balanceOf(userAddress);
      const UTTBalance = Number(ethers.utils.formatUnits(walletUTTBalance, 0) || 0);
      setUTTWalletState(UTTBalance);
    } catch (e) {
      console.error(e);
      setUTTWalletState(0);
    }
  }

  useEffect(() => {
    fetchBalance();
  }, []);


  return (
    <BaseComponent style={style} className="utt_balance">
      <h3>{UTTWalletState}</h3>
    </BaseComponent>
  );
}