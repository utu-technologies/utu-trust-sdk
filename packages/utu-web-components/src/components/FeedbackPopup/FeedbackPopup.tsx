/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import PopUp from "../PopUp";
import { BaseComponent } from "../BaseComponent";
import FeedbackDetails from "../FeedbackDetails";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import style from "./FeedbackPopup.scss";
import {ATTR_BTN_COLOR, ATTR_THEME_COLOR} from "../../names";

export default function FeedbackPopup(props: IFeedbackDetailsProps) {
  
  const [popUpVisible, setPopUpVisible] = useState(false);

  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition = isDark ? "dark" : "light"

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  return <BaseComponent style={style} 
    className={`x-utu-feedback-details-popup x-utu-section x-utu-section-no-border-${envCondition}`}>
    <button type="button"
      style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
      className={`x-utu-btn x-utu-btn-${envCondition} border-radius`} onClick={() => setPopUpVisible(true)}>
        Show Feedback Details
    </button>
    {popUpVisible && 
      <PopUp onClose={() => setPopUpVisible(false)} {...props}>
        <FeedbackDetails {...props} />
      </PopUp>
    }
  </BaseComponent>;
}
