/* eslint-disable no-nested-ternary */
import { h } from "preact";
import {SubmitStatus} from "../../hooks/api";
import {FlowerPenguin, SadPenguin} from "../Images";

export interface ISubmitStatusViewProps {
  children: JSX.Element;
  submitStatus: SubmitStatus;
}

/**
 * A component which renders the given child when the provided SubmitStatus is idle, a spinner when submitting
 * a success message on success and error message on error.
 *
 * @param props
 * @constructor
 */
export default function SubmitStatusView(props: ISubmitStatusViewProps) {
  const { children, submitStatus } = props;

  switch (submitStatus) {
    case SubmitStatus.idle: return children;
    case SubmitStatus.submitting: return <div className="spinner"/>;
    case SubmitStatus.success: return <div className="submit-success"><FlowerPenguin/>Thank you!</div>;
    default: return <div className="submit-error"><SadPenguin/>Error.</div>;
  }
}
