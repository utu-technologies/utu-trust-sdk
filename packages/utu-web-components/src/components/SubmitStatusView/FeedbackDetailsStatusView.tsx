import {h} from "preact";
import "./StatusView.scss"
import {FlowerPenguin, SadPenguin} from "../Images";
import {SubmitStatus} from "../../hooks/api";

export interface IFeedbackDetailsStatusViewProps {
  children: JSX.Element;
  submitStatus: SubmitStatus;
  // eslint-disable-next-line react/require-default-props
  msg?: string;
}

export default function FeedbackDetailsStatusView(props: IFeedbackDetailsStatusViewProps) {
  const {children, submitStatus, msg=""} = props;

  switch (submitStatus) {
    case SubmitStatus.idle:
      return (<div className="description">
        <div className="icon skeleton"/>

        <div className="details">
          <div className="title skeleton"/>
          <div className="information skeleton"/>
        </div>
      </div>
      );
    case SubmitStatus.submitting:
      return (<div className="description">
        <div className="icon skeleton"/>

        <div className="details">
          <div className="title skeleton"/>
          <div className="information skeleton"/>
        </div>
      </div>
      );
    case SubmitStatus.success:
      return children;
    default:
      return (<div className="description">
        <div className="details">
          <div className="title skeleton">{msg}</div>
          <div className="information skeleton"/>
        </div>
      </div>
      );
  }
}