/* eslint-disable no-nested-ternary */
import {h} from "preact";
import {useState} from "preact/hooks";
import PopUp from "../PopUp";
import {BaseComponent} from "../BaseComponent";
import FeedbackForm from "../FeedbackForm/FeedbackForm";
import {IFeedbackProps} from "../FeedbackForm/FeedbackProps";
import style from "../FeedbackForm/FeedbackForm.scss";
import {ATTR_BTN_COLOR, ATTR_THEME_COLOR} from "../../names";

export default function FeedbackFormPopup(props: IFeedbackProps) {
  const [popUpVisible, setPopUpVisible] = useState(false);
  
  // environments
  const [isDark, setIsDark] = useState(false);
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }
  // environment conditionals
  const envCondition = isDark ? "dark" : "light"

  return <BaseComponent style={style}
    className={`x-utu-feedback-form-popup x-utu-section x-utu-section-no-border-${envCondition}`}>
    <button type="button"
      style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
      className={`x-utu-btn x-utu-btn-${envCondition} border-radius`} onClick={() => setPopUpVisible(true)}>
          Give feedback
    </button>
    {popUpVisible && (
      <PopUp onClose={() => setPopUpVisible(false)} {...props}>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <FeedbackForm {...props} />
      </PopUp>
    )}
  </BaseComponent>
}
