import { IBaseOwnProps } from "../BaseComponent";
import {
  ATTR_ENDORSEMENT_NETWORK,
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID,
  ATTR_TRANSACTION_ID,
  NETWORK_NAME
} from "../../names";

const x = "x"
export interface IFeedbackProps extends IBaseOwnProps {
  [ATTR_SOURCE_UUID]: string;
  [ATTR_TARGET_UUID]: string;
  [ATTR_TRANSACTION_ID]: string;
  [ATTR_ENDORSEMENT_NETWORK]: NETWORK_NAME;
}
