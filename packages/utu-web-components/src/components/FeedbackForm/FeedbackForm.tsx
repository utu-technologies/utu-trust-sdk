import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../BaseComponent";
import { IFeedbackProps } from "./FeedbackProps";

// styling
import style from "./FeedbackForm.scss";
// components
import { Badges } from "../Badges";
import { FeedbackTextInput } from "../FeedbackTextInput";
import { EndorsementForm } from "../EndorsementForm";
// import { EndorsementsList } from "../EndorsementsList";
import { StarRatingInput } from "../StarRatingInput";
// import RecordVideoContainer from "../RecordVideo/RecordVideoContainer";
import Logo from "../Images"
import {ATTR_THEME_COLOR} from "../../names";
import RecordVideo from "../RecordVideo/RecordVideo";

export default function FeedbackForm(props: IFeedbackProps) {


  // environments
  const [isDark, setIsDark] = useState(false);
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }
  /* eslint-disable react/jsx-props-no-spreading */
  return (
    <BaseComponent style={style}
      className="utu-feedback-form">

      <div className="mobile-view">
        <RecordVideo {...props}/>
        <StarRatingInput {...props}/>
        <Badges {...props}/>
        <FeedbackTextInput {...props}/>
        <EndorsementForm {...props}/>
        <section className="logo-section-mobile">
          <div className="logo-position-mobile"><Logo /></div>
        </section>
      </div>

      <div className="desktop-view">
        {/* <div className="desktop-view-block-1"> */}
        <div className={`desktop-view-block-1 desktop-view-block-1-${
          isDark ? "dark" : "light"
        }`
        }>
          <RecordVideo {...props}/>
          <div className="x-utu-divider" />
          <StarRatingInput {...props}/>
        </div>
        <div className="x-utu-feedback-form-Badges">
          <Badges {...props}/>
        </div>
        {/* <div className="desktop-view-block-2"> */}
        <div className={`desktop-view-block-2 desktop-view-block-2-${
          isDark ? "dark" : "light"
        }`
        }>
          <FeedbackTextInput {...props}/>
        </div>
        {/* <div className="desktop-view-block-3"> */}
        <div className={`desktop-view-block-3 desktop-view-block-3-${
          isDark ? "dark" : "light"
        }`
        }>
          <EndorsementForm {...props}/>
        </div>
        <section className="logo-section">
          <div className="logo-position"><Logo /></div>
        </section>
      </div>


    </BaseComponent>
  );
}


