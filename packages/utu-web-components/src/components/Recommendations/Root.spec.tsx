import { h } from "preact";
import { mount } from "enzyme";
import { useState } from "preact/hooks";
import Root, { IDataElement } from "./Root";
import * as hooks from "../../hooks/api";

jest.mock("../../hooks/api");

describe("Root", () => { 
  it("should be defined", async () => {
    const expected: hooks.IRankingItem[] = [];

    let dataLoadedPromiseResolve: (value: hooks.IRankingItem[]) => void;
    const dataLoadedPromise = new Promise<hooks.IRankingItem[]>(
      resolve => { dataLoadedPromiseResolve = resolve; }
    );

    jest.spyOn(hooks, "useRankingApi").mockImplementation(() => {
      const [rankingItems, setRankingItems] = useState<hooks.IRankingItem[]>(
        null
      );

      setTimeout(() => {
        setRankingItems(expected);
      }, 0);

      return { rankingItems };
    });

    const wrapper = mount(
      <Root api-key="123" css-url="css-url" api-url="hello" source-uuid="123" target-type="provider" />
    );
    const element: IDataElement = wrapper.getDOMNode();

    element.addEventListener("loaded", (event: CustomEvent) => {
      dataLoadedPromiseResolve(event.detail.data);
    });

    const initialActual = element.data;
    expect(initialActual).toBeUndefined();

    const actualPromiseResult = await dataLoadedPromise;
    expect(actualPromiseResult).toBe(expected);

    const finalActual = element.data;
    expect(finalActual).toBe(expected);
  });
});
