/* eslint-disable no-nested-ternary */
import { Fragment, h } from "preact";
import { useEffect, useRef, useState, Ref } from "preact/hooks";
import { IRankingItem } from "../../hooks/api";
import { IDataElement } from "./Root";
import { BaseComponent } from "../BaseComponent";
import {TAG_ROOT, ATTR_TARGET_UUID, ATTR_THEME_COLOR} from "../../names";

import style from "./Recommendation.scss";

export interface IOwnProps {
  [ATTR_TARGET_UUID]: string;
  [ATTR_THEME_COLOR]:string;
}

function getRoot(ref: Ref<HTMLDivElement>): IDataElement | null {
  if (ref && ref.current) {
    const shadowRoot = ref.current.getRootNode() as {
      host: HTMLElement;
    } & Node;
    const rootContainer = shadowRoot.host.closest(TAG_ROOT);

    if (rootContainer) {
      return (rootContainer.shadowRoot.firstChild as unknown) as IDataElement;
    }
  }

  return null;
}

function getValueFromRoot(ref: Ref<HTMLDivElement>, targetUuid: string) {
  const root = getRoot(ref);

  if (root && root.data) {
    return root.data.find(item => item.entity.uuid === targetUuid);
  }

  return null;
}

export default function Recommendation(props: IOwnProps) {
  const ref = useRef<HTMLDivElement>();
  const targetUuid = props[ATTR_TARGET_UUID];

  // environments
  const [isDark, setIsDark] = useState(false);
  // const [isDark, setIsDark] = useState(false);




  const [rankingItem, setRankingItem] = useState<IRankingItem>(
    getValueFromRoot(ref, targetUuid)
  );

  if(props[ATTR_THEME_COLOR] === "dark"){
    setIsDark(true);
  }

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"


  useEffect(() => {
    function loaded() {
      setRankingItem(getValueFromRoot(ref, targetUuid));
    }

    if (ref.current && targetUuid) {
      const root = getRoot(ref);

      if (root) {
        root.addEventListener("loaded", loaded);
      }
    }

    return () => {
      const root = getRoot(ref);

      if (root) {
        root.removeEventListener("loaded", loaded);
      }
    };
  }, [ref, targetUuid]);


  return (
    <BaseComponent
      style={style}
      forwardedRef={ref}
      className={`utu-recommendation x-utu-section x-utu-section-no-border-${envCondition}`}

    >
      {rankingItem && (
        <Fragment>
          <div className="utu-recommendation-content">
            <ul className="summary-image-list">
              {rankingItem.summaryImages.map(image => (
                <li key={image}>
  
                  <img
                    className={`rounded-circle summary-image-item summary-image-item-${envCondition}`}
                    src={image}
                    onError={(e) => {
                      (e.target as HTMLImageElement).onerror = null;
                      (e.target as HTMLImageElement).src = "";
                      (e.target as HTMLImageElement).className = "";
                      (e.target as HTMLImageElement).alt = "";


                    }}
                    alt="profile"
                  />

                </li>
              ))}
            </ul>
            <div className={`utu-recommendation-text utu-recommendation-text-${envCondition}`}>{rankingItem.summaryText}</div>
          </div>
        </Fragment>
      )}
    </BaseComponent>
  );
}
