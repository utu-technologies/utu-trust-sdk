/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import {omit} from "lodash";
import { BaseComponent } from "../BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../names";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import style from "./BadgesShow.scss";
import FeedbackDetailsStatusView from "../SubmitStatusView/FeedbackDetailsStatusView";


export default function BadgesShow(props: IFeedbackDetailsProps) {
  
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
   isDark ? "dark" : "light"


  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  if (!props.feedbackSummary) {
    return  (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div /></FeedbackDetailsStatusView>);
  }

  // if (!props.feedbackSummary) {
  //   return  <Fragment />;
  // }


  // fet badges and keys
  const {summaryText} = props.feedbackSummary.badges;
  const fbBadges = props.feedbackSummary.badges;
  const Badge = omit(fbBadges,"summaryText");

  const keys = Object.keys(Badge)


  // returns the badge with image, thumb and title
  const badgeGroup = keys.map((key, i) => {
    const badges = Badge[key];
    const badgeKeys = Object.keys(badges);

    return badgeKeys.map((badgeKey: string) =>
      <div key={badgeKey} className="badge-spacing">
        <div className="badge-container">
          <img 
            className={`badge-container-thumb badge-container-thumb-${envCondition} badge-container-position-child-thumb`} 
            src={badges[badgeKey].qualifiers.positive.badge.image} alt="person face" />
          <img className="badge-container-img" src={badges[badgeKey].image} alt="person face" />
          <div className="badge-container-profile-container">
            { badges[badgeKey].qualifiers.positive.summaryImages instanceof Array ?
              badges[badgeKey].qualifiers.positive.summaryImages.map(
                (img: string) => ( 
                  <img className={`badge-container-profile badge-container-profile-${envCondition} badge-container-position-child-profile`} 
                    src={img} alt="person face"/> )) : "" 
            } 
          </div>
          <div className={`badge-container-title badge-container-title-${envCondition}`}>{badgeKey}</div>
        </div>
      </div>)
  })


  const badgeGroupNoData = (
    <div className="badge-container-no-data-container">
      <div className="badge-container-no-data">
        <img className="badge-container-img badge-container-img-no-data" 
          src="https://utu-trust-sdk.s3.eu-central-1.amazonaws.com/v2/badge-sets/Product.svg" alt="badge product" />
        <div className={`badge-container-title badge-container-title-${envCondition}`}>Product</div>
      </div>
      <div className="badge-container-no-data">
        <img className="badge-container-img badge-container-img-no-data" 
          src="https://utu-trust-sdk.s3.eu-central-1.amazonaws.com/v2/badge-sets/Quality.svg" alt="badge quality" />
        <div className={`badge-container-title badge-container-title-${envCondition}`}>Quality</div>

      </div>
      <div className="badge-container-no-data">
        <img className="badge-container-img badge-container-img-no-data" 
          src="https://utu-trust-sdk.s3.eu-central-1.amazonaws.com/v2/badge-sets/Product.svg" alt="person face" />
        <div className={`badge-container-title badge-container-title-${envCondition}`}>Product</div>
      </div>
    </div>
  )

  return (
    <BaseComponent
      style={style}
      className={`badge-section x-utu-section-no-border-${
        isDark ? "dark" : "light"
      }`
      }
      excludeBootstrap
      excludeFonts
    >
      <FeedbackDetailsStatusView submitStatus={props.submitStatus}>
        {Object.keys(fbBadges.assignments).length !== 0 ? 
 

          <section className={`badge-section-content x-utu-section x-utu-section-${envCondition}`}>   
            <div className="badge-section-badges">{badgeGroup}</div>
            <div className="badge-section-text">
              <p className="badge-section-text-content">
                {summaryText}
              </p>
            </div>
          </section>
          :           
          <section className={`badge-section-content x-utu-section x-utu-section-${envCondition}`}>   
            <h3 className="badge-section-title">{fbBadges.summaryText}</h3>
            <div className="badge-section-badges-no-data">{badgeGroupNoData}</div>
          </section> 
        }
      </FeedbackDetailsStatusView>
    </BaseComponent>
  );
}
