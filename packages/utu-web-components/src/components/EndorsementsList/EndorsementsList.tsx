/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent, IBaseOwnProps, config } from "../BaseComponent";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import {
  ATTR_THEME_COLOR,
  ATTR_SOURCE_UUID
} from "../../names";
import Avatar from "../FeedbackDetails/Avatar";
import style from "./EndorsementsList.scss";
import {IEndorsements, IEntity} from "../../hooks/api";
import FeedbackDetailsStatusView from "../SubmitStatusView/FeedbackDetailsStatusView";

export type IOwnProps = IBaseOwnProps;
// const minLength = 5;

const CONNECTION_TYPE_REF: Record<string, string> = {
//  twitter: "Your Twitter connection",
//  telegram: "Your Telegram contact"
  twitter: "Twitter user",
  telegram: "Telegram user"
}

const DEFAULT_CONNECTION_REF = "User";

interface ApiConnectionData {
  name: string | null,
  image?: string
}

interface ConnectionInfo {
  ref: string;
  name?: string;
}

function getConnectionInfo(entity: IEntity) : ConnectionInfo {
  if(!entity) return { ref: "Someone" };

  // eslint-disable-next-line no-restricted-syntax
  for(const key in CONNECTION_TYPE_REF) {
    if(entity.properties[key]) {
      return {
        ref: CONNECTION_TYPE_REF[key],
        name: (entity.properties[key] as ApiConnectionData).name
      }
    }
  }

  // No connection type was found, return default
  return { ref: DEFAULT_CONNECTION_REF,  name: entity.name }
}

const SELF_INFO : ConnectionInfo = {
  ref: "You"
}

function getEndorsementTextBody(userUuid: string, endorsementInfo: IEndorsements) {
  const endorser = endorsementInfo.source;
  const isSelf = endorser.uuid === userUuid;
  const connectionInfo = isSelf ? SELF_INFO : getConnectionInfo(endorser);
  const has = isSelf ? "have" : "has";

  return (<p className="endor-text-contacts-body ">
    {connectionInfo.ref}&nbsp;
    { connectionInfo.name?.length > 0 &&
      <span className="endor-text-truncate"><b>{connectionInfo.name}</b>&nbsp;</span>
    }
    {has} endorsed <b className="mx-1">{ endorsementInfo.endorsement?.value } UTT</b>
  </p>);
}

export default function EndorsementsList(props: IFeedbackDetailsProps) {
  // environments
  const [isDark, setIsDark] = useState(false);

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"

  if (!props.feedbackSummary) {
    return  (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div /></FeedbackDetailsStatusView>);
  }

  const {endorsements} = props.feedbackSummary;

  if (!endorsements) {
    return  (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div /></FeedbackDetailsStatusView>);
  }

  const displayEndorsements = endorsements.slice(0, config.feedbackDetailsMaxEndorsements);

  // image
  const img: any = displayEndorsements.map(i => i.source?.image);
  const endorImg = ()=>{
    return <img
      className={`endor-text-image-item endor-text-image-item-${envCondition}`}
      src={img}
      alt="endorsment"
      onError={e => {
        (e.target as HTMLImageElement).onerror = null;
        (e.target as HTMLImageElement).src = "";
        (e.target as HTMLImageElement).className = "";
        (e.target as HTMLImageElement).alt = "";
      }}
    />
  }

  return <BaseComponent
    style={style}
    className={`endor-text-input-section utu-text-section-${envCondition}`}
    excludeBootstrap excludeFonts>
    <FeedbackDetailsStatusView submitStatus={props.submitStatus}>

      <section className={`endor-text-section x-utu-section-${envCondition}`}>
        <h3 className="endor-title">endorsements</h3>
        <div className={`endor-text endor-text-${envCondition}`}>
          {endorsements.length > 0 ? displayEndorsements.map((i: IEndorsements) => (
            <div className="endor-text-body">
              <div className="endor-text-img">
                {endorImg()}
              </div>

              <div className="endor-text-contacts-section">
                {getEndorsementTextBody(props[ATTR_SOURCE_UUID], i)}
              </div>

              <div className="endor-text-stakes">
                <div className="endor-text-stakes-item"><b>{i.endorsement?.value}</b></div>
                <div className="endor-text-stakes-item-2"><b>staked</b></div>
              </div>
            </div>
          )) : 
            <div className="endor-text-stakes-no-data"> 
              <div className={`ml-5 no-data-avatar rounded-circle avatar-no-img-${envCondition}`}>
                <Avatar {...props} />
              </div>
              <div>
                there are no endorsements to show
              </div>
            </div>
          }
        </div>
    
      </section>
    </FeedbackDetailsStatusView>
  </BaseComponent>;
}
