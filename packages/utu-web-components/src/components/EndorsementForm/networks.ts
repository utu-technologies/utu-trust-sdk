import { ethers } from "ethers";

import { NETWORK_NAME } from "../../names";
import { config as SDKConfig } from "../BaseComponent/BaseComponent";

/**
 * Parameter interface definition for wallet_addEthereumChain
 * Copied from https://docs.metamask.io/guide/rpc-api.html#unrestricted-methods
 */
interface AddEthereumChainParameter {
  chainId: string; // A 0x-prefixed hexadecimal string
  chainName: string;
  nativeCurrency: {
    name: string;
    symbol: string; // 2-6 characters long
    decimals: number;
  };
  rpcUrls: string[];
  blockExplorerUrls?: string[];
  iconUrls?: string[]; // Currently ignored.
}

/**
 * Parameter interface definition for wallet_switchEthereumChain
 * Copied from https://docs.metamask.io/guide/rpc-api.html#wallet-switchethereumchain
 */
interface SwitchEthereumChainParameter {
  chainId: string; // A 0x-prefixed hexadecimal string
}

/**
 * Convers a number to a 0x-prefixed hexadecimal string
 * @param n
 */
function toHexString(n: number) {
  return ethers.utils.hexStripZeros(ethers.utils.hexlify(n));
}

const SWITCH_CHAIN_PARAMS: { [key: string]: SwitchEthereumChainParameter } = {
  [NETWORK_NAME.polygon_mainnet]: { chainId: toHexString(137) },
  [NETWORK_NAME.polygon_mumbai]: { chainId: toHexString(80001) }
}

const ADD_CHAIN_PARAMS: { [key: string]: AddEthereumChainParameter } = {
  [NETWORK_NAME.polygon_mainnet]: {
    chainId: SWITCH_CHAIN_PARAMS[NETWORK_NAME.polygon_mainnet].chainId,
    chainName: "Polygon",
    nativeCurrency: { name: "MATIC", symbol: "MATIC", decimals: 18 },
    rpcUrls: ["https://polygon-rpc.com"],
    blockExplorerUrls: ["https://polygonscan.com"]
  },
  [NETWORK_NAME.polygon_mumbai]: {
    chainId: SWITCH_CHAIN_PARAMS[NETWORK_NAME.polygon_mumbai].chainId,
    chainName: "Mumbai",
    nativeCurrency: { name: "MATIC", symbol: "MATIC", decimals: 18 },
    rpcUrls: ["https://rpc-mumbai.maticvigil.com"],
    blockExplorerUrls: ["https://mumbai.polygonscan.com"],
  }
};

const UTT_CONTRACT_ADDRESS : { [key: string]: string } = {
  [NETWORK_NAME.polygon_mainnet]: "0xbA6de5f4a98d2AEbb289d1136194D05Ee24C426f",
  [NETWORK_NAME.polygon_mumbai]: "0x19bc90FfBDCaD53c48eF0b08A67B0D2563AEE2a8"
}

export async function addNetwork(provider: any, networkName: NETWORK_NAME) : Promise<void> {
  const network = ADD_CHAIN_PARAMS[networkName];
  await provider.request({
    method: "wallet_addEthereumChain",
    params: [network]
  });
}

export async function requestNetworkChange(provider: any, networkName: NETWORK_NAME) : Promise<void> {
  const network = SWITCH_CHAIN_PARAMS[networkName];
  await provider.request({
    method: "wallet_switchEthereumChain",
    params: [network]
  });
}

export function getDefaultNetworkName() : NETWORK_NAME  {
  return SDKConfig.production ? NETWORK_NAME.polygon_mainnet : NETWORK_NAME.polygon_mumbai;
}

export function getUTTContractAddress(networkName: NETWORK_NAME) {
  return UTT_CONTRACT_ADDRESS[networkName];
}

export function getProvider() {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return window.utuWeb3Provider || window.ethereum;
}