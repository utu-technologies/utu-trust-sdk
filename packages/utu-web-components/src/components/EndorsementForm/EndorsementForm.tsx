import { h } from "preact";
import { useState } from "preact/hooks";
import { useForm } from "react-hook-form";
import {Contract, ethers, Transaction} from "ethers";
import { BaseComponent, IBaseOwnProps } from "../BaseComponent";
import { abi as UTTAbi } from "../../contracts/UTT.abi.json";
import {
  ATTR_BTN_COLOR,
  ATTR_ENDORSEMENT_NETWORK,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID, NETWORK_NAME
} from "../../names";

import style from "./EndorsementForm.scss";
import { IFeedbackProps } from "../FeedbackForm/FeedbackProps";
import {
  addNetwork,
  requestNetworkChange,
  getDefaultNetworkName,
  getUTTContractAddress
} from "./networks"
import {UttBalance} from "../UttBalance";
import {UttLogo} from "../Images";

export type IOwnProps = IBaseOwnProps;

export default function EndorsementForm(props: IFeedbackProps) {

  // environments
  const [isDark, setIsDark] = useState(false);
  const [txError, setTxError] = useState("");

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"

  const { register, errors, handleSubmit } = useForm();

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  if (!(window?.ethereum || window?.web3 || window.utuWeb3Provider)
    || !props[ATTR_TARGET_UUID]
    || !ethers.utils.isAddress(props[ATTR_TARGET_UUID])
    || !props[ATTR_TRANSACTION_ID]) return null;

  function getProvider() {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return window.utuWeb3Provider || window.ethereum;
  }

  /**
   * Determines which network to use for endorsements.
   */
  function getEndorsementNetwork() : NETWORK_NAME {
    return props[ATTR_ENDORSEMENT_NETWORK] ?? getDefaultNetworkName();
  }

  /**
   * Switches to the given network, adding it if necessary.
   */
  async function switchNetwork(network : NETWORK_NAME){

    const provider = getProvider();

    try {
      await requestNetworkChange(provider, network);
    } catch (e) {
      console.log(e, e.message);
      if (e.code === 4902 || e.message.toLowerCase().includes("unrecognized")) {
        await addNetwork(provider, network);
        await requestNetworkChange(provider, network);
      }
    }
  }

  /**
   * return the UTU contract instance on the given network
   */
  async function getContractInstance(network : NETWORK_NAME) : Promise<Contract> {
    const UTT_CONTRACT_ADDRESS = getUTTContractAddress(network);
    const web3Provider = await new ethers.providers.Web3Provider(getProvider());
    const signer = web3Provider.getSigner()
    return new ethers.Contract(UTT_CONTRACT_ADDRESS, UTTAbi, signer)
  }

  /**
   * returns the current connected address
   */
  async function getConnectedAddress() : Promise<string> {
    const web3Provider = await new ethers.providers.Web3Provider(getProvider());
    const signer = web3Provider.getSigner()
    return signer.getAddress()
  }

  /**
   * Creates an endorsement transaction
   * @param address the target address of the endorsement
   * @param amount the amount of UTT to stake on the endorsement
   * @param transactionId the business transaction id (might or might not be an Ethereum tx id) to record with the
   *      endorsement, if any
   */
  async function endorse(address: string, amount: string, transactionId: string) : Promise<Transaction> {
    const network = getEndorsementNetwork();
    await switchNetwork(network);
    const contract = await getContractInstance(network);
    const connectedAddress = await getConnectedAddress()
    const utuBalance = await contract.balanceOf(connectedAddress)
    if(Number(utuBalance) < Number(amount)){
      throw new Error("Insufficient UTU tokens");
    }
    const transaction = await contract.endorse(address, String(amount), transactionId)
    await transaction.wait()
    return transaction;
  }

  async function onSubmit({ amount }: { amount: string }) {
    if (!amount) return;
    setTxError("");
    try {
      await endorse(props[ATTR_TARGET_UUID], amount, props[ATTR_TRANSACTION_ID])
    } catch (error) {
      console.log(error);
      setTxError(error.data ? error.data.message : error.message);
    }
  }

  const showError = ({ amount }: any) => {
    switch (amount?.type) {
      case "required":
        return "Supply a valid token value."
      case "min":
        return "Minimum value supplied should be 1";
      case "integer":
        return "Value supplied must be an integer."
      default:
        return "";
    }
  }


  return <BaseComponent
    style={style}
    className={`endor-input-section x-utu-section x-utu-section-no-border-${envCondition}`}
    excludeBootstrap excludeFonts>
    <section className="endor-input-form-section">
      <div className="endor-utt-balance">
        <UttLogo/>
        <div className="x-utu-utt-heading-text">
          <h3>UTT Balance</h3>
          <UttBalance/>
        </div>
      </div>
      <div className={`endor-text endor-text-${envCondition}`}>

        <div className="endor-text-area">
          <p className="endor-text-title">Would you like to Endorse ?</p>
          <p
            className="endor-text-info"
            data-bs-toggle="tooltip"
            title="
            To endorse, you need to have some UTT
            in your account. Make sure you have a
            sufficient balance in your account"
          >i</p>
        </div>

        <p className="endor-text-body">
          When you endorse a platform, it means
          you have personally had a good experience
          and want to back your rating by staking
          an amount of UTT (UTU trust token) on
          the platform.</p>
      </div>

      <form onSubmit={handleSubmit(onSubmit) as () => void} className="endor-input-form">
        <div style={{ flex: 1, marginRight: 20 }}>
          <input
            className={`endor-input-form-text-area endor-input-form-text-area-${envCondition}`}
            ref={register({ required: true, minLength: 1 })}
            min="0" type="number" name="amount" placeholder="Your endorsement" />

          <div className="mt-1 error">
            {errors.amount && `* ${showError(errors)}`}
          </div>
          {
            txError && <div className="mt-1 error">{txError}</div>
          }
        </div>
        <div>
          <button
            style={{ backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}` }}
            className={`endor-btn x-utu-btn x-utu-btn-${envCondition}`} type="submit">Endorse
          </button>
        </div>
      </form>
    </section>
  </BaseComponent>;
}
