/* eslint max-len: 0 */
/* eslint-disable no-nested-ternary */

import { h } from "preact";
import { useState } from "preact/hooks";
import style from "../../../index.scss";


export default function RecordIcon() {

    // environments
    const [isDark, setIsDark] = useState(true);


    return <svg
        className={`video-icon video-icon-${isDark ? "dark" : "light"
            }`} viewBox="0 0 512 512">
        {/* <!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --> */}
        <path d="M512 256c0 141.4-114.6 256-256 256S0 397.4 0 256S114.6 0 256 0S512 114.6 512 256zM256 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96zm0 32c70.7 0 128-57.3 128-128s-57.3-128-128-128s-128 57.3-128 128s57.3 128 128 128zm0-96c17.7 0 32-14.3 32-32s-14.3-32-32-32s-32 14.3-32 32s14.3 32 32 32z" /></svg>
}

