/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
// import Loader from "react-loader-spinner";
import {
  ATTR_BTN_COLOR, ATTR_THEME_COLOR
} from "../../names";
import { BaseComponent } from "../BaseComponent";
import { IFeedbackProps } from "../FeedbackForm/FeedbackProps";
import Recorder from "./Recorder";
import style from "./RecordVideo.scss";
import VideoIcon from "./icons/videoIcon";

export default function RecordVideo(props: IFeedbackProps) {
  /**
   * Holds the active reader retrieved from ReadableStorage.getReader() while a video is recording, or null while no
   * video is recording.
   */
  const [videoVisible, setVideoVisible] = useState(false);

  // environments
  const [isDark, setIsDark] = useState(false);


  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }


  // environment conditionals
  const envCondition =
    isDark ? "dark" : "light"

  return (
    <BaseComponent style={style} className={`x-utu-video-container x-utu-section x-utu-section-${envCondition}`}>
      <div className="x-utu-video-wrapper">
        <button
          type="button"
          style={{ backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}` }}
          className={`x-utu-video-btn-round x-utu-video-btn-round-${envCondition}`}
          onClick={() => {
            setVideoVisible(true)
          }}
        >
          <VideoIcon />
        </button>
        <p className={`mx-3 mt-3 x-utu-video-btn-round-text-${envCondition}`}> <b>RECORD YOUR STORY</b> </p>
        {videoVisible && (<Recorder {...props} onClose={()=> {
          setVideoVisible(false)
        }} />)}
      </div>
    </BaseComponent>
  );
}
