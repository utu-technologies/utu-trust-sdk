import { h } from "preact";
import {useEffect, useLayoutEffect, useRef, useState} from "preact/hooks";
import "./Player.scss"
import Logo from "../../Images"


export default (videoUrl: string, preview?: boolean, previewStream?: MediaStream )=>{
  const [controlsVisible, setControlsVisible] = useState(false);

  const videoRef = useRef<HTMLVideoElement>(null);
  const playBtnRef = useRef(null)
  const muteBtnRef = useRef(null)

  // const [size, setSize] = useState({width: 0, height: 0});

  useLayoutEffect(() => {
    function updateSize() {
      // setSize({width: window.innerWidth, height: window.innerHeight});
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  const play = async ()=> {
    getDuration();
    if (videoRef.current.paused){
      await videoRef.current.play();
      playBtnRef.current.classList.add("paused")
    }
    else {
      videoRef.current.pause();
      playBtnRef.current.classList.remove("paused")
    }
  }

  const getDuration = ()=> {
    let dur = videoRef.current.duration;
    dur = Number(dur.toFixed());
    // const formatted = "";

    // setLength(dur);
    // setFormattedLength(formatted);

    return dur;
  }

  const mute = ()=> {
    videoRef.current.muted = !videoRef.current.muted;

    if(videoRef.current.muted) {
      muteBtnRef.current.classList.add("off")
    }  else {
      muteBtnRef.current.classList.remove("off")
    }

  }

  const fullScreen = async () => {
    await videoRef.current.requestFullscreen()
  }

  useEffect(()=>{
    if (videoRef.current)  {
      setControlsVisible(true);
    }

  }, [videoUrl])


  const Preview = ({ stream }: { stream: MediaStream | null }) => {
    const previewRef = useRef<HTMLVideoElement>(null);

    useEffect(() => {
      if (previewRef.current && stream && preview) {
        previewRef.current.srcObject = stream;
      }
    }, [stream]);

    if (!stream) {
      return null;
    }

    if (!stream.active) {
      return (
        <div className="trust-video-msg">
          <p>No video preview to show at the moment. Please hold on!!</p>
        </div>
      )
    }

    return (
      <video className="trust-video preview-video" id="trustVideoPreview" ref={previewRef} autoPlay>
        <track kind="captions"  />
      </video>
    )

  };

  const VideoSrc = () => {
    if(videoUrl) {
      return (
        <video className="trust-video" id="trustVideo" ref={videoRef}  autoPlay >
          <source className="trust-video"src={videoUrl} type="video/mp4" />
          <source src={videoUrl} type="video/webm" />
          <track kind="captions"  />
        </video>);
    }

    return <Preview stream={previewStream} />;
  };

  return (
    <div className="trust-video-wrapper">
      <VideoSrc />

      <div className={`trust-video-controls ${ controlsVisible && "show"}` }>
        <button
          ref={muteBtnRef}
          type="button"
          className={`trust-video-volume trust-video-control ${(videoRef.current && videoRef.current.muted) && "off"}`}
          onClick={mute} > </button>

        {
          !preview &&
          <button
            ref={playBtnRef}
            type="button"
            className="trust-video-play trust-video-control paused"
            onClick={play} > </button>
        }
        <div className="trust-video-progress" />
        {
          !preview && <button type="button" className="trust-video-fullscreen trust-video-control" onClick={fullScreen}  > </button>
        }
      </div>
      <section className="logo-video-section">
        <div className="logo-video-position"><Logo /></div>
      </section>
    </div>
  )
}
