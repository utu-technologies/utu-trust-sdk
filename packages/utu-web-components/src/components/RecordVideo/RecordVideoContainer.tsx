/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState} from "preact/hooks";
import {
} from "react-media-recorder";
import { BaseComponent } from "../BaseComponent";
import {
  ATTR_THEME_COLOR,
  ATTR_BTN_COLOR

} from "../../names";
import style from "./RecordVideo.scss";
import { IFeedbackProps } from "../FeedbackForm/FeedbackProps";
import VideoIcon from "./icons/videoIcon";
import PopUp from "../PopUp/PopUp";
// import RecordVideo from "./RecordVideo";
import RecordVideo from "./RecordVideo";


export default function Recorder(props: IFeedbackProps) {
  /**
   * Holds the active reader retrieved from ReadableStorage.getReader() while a video is recording, or null while no
   * video is recording.
   */
  const [videoVisible, setVideoVisible] = useState(false);

  // environments
  const [isDark, setIsDark] = useState(false);


  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }


  // environment conditionals
  const envCondition =
    isDark ? "dark"  : "light"

  return (
    <BaseComponent style={style} className={`x-utu-video-container x-utu-section-${envCondition}`}>
      <div>
        <div className="d-flex justify-content-center">
          <div className="x-utu-video-container">
            <button
              type="button"
              style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
              className={`x-utu-video-round x-utu-video-round-${envCondition}`}
              onClick={() => {
                setVideoVisible(!videoVisible)
              }}
            >
              <VideoIcon />
              <p  className={`x-utu-video-round-text-${envCondition}`} >RECORD YOUR STORY</p>
            </button>
          </div>
          {videoVisible &&
                <PopUp closeButtonOverlay onClose={() => setVideoVisible(false)}>
                  <RecordVideo {...props} />
                </PopUp>
          }    
        </div>
      </div>
    </BaseComponent>
  );
}
