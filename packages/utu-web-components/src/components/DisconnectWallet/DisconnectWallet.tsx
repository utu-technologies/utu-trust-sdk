import { h } from "preact";
import { BaseComponent } from "../BaseComponent";
import style from "./DisconnectWallet.scss";
import { useWalletConnectApi } from "../../hooks/api";

export default function DisconnectWallet() {
  const {web3Modal} = useWalletConnectApi();


  async function disconnectWallet() {
    const provider = await web3Modal.cachedProvider;
    await web3Modal.clearCachedProvider();
    if (provider?.disconnect && typeof provider.disconnect === "function") {
      await provider.disconnect();
    }
    if (provider?.close && typeof provider.close === "function") {
      await provider.close();
    }
    await localStorage.removeItem("walletconnect");
    window.location.reload();
  }
  return(
    <BaseComponent style={style}>
      <section className="x-utu-disconnect-wallet-btn">
        <div>
          <button
            onClick={disconnectWallet}
            className="x-utu-disconnect-text-input-btn x-utu-btn" type="submit">
          Disconnect Wallet
          </button>
        </div>
      </section>
    </BaseComponent>
  )
}

