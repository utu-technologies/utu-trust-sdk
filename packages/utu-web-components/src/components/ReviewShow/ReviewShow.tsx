/* eslint-disable no-nested-ternary */
import { Fragment, h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../names";
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import style from "./ReviewShow.scss";
import Avatar from "../FeedbackDetails/Avatar";
import FeedbackDetailsStatusView from "../SubmitStatusView/FeedbackDetailsStatusView";


export default function BadgesShow(props: IFeedbackDetailsProps) {
  
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
   isDark ? "dark" : "light"


  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  if (!props.feedbackSummary) {
    return  <Fragment />;
  }

  // if (!props.feedbackSummary) {
  //   return  (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div /></FeedbackDetailsStatusView>);
  // }

  // if (!props.feedbackSummary.reviews || props.feedbackSummary.reviews.length === 0) {
  //   return  (<FeedbackDetailsStatusView submitStatus={3} msg="No reviews available"><div></div></FeedbackDetailsStatusView>);
  // }


  // review
  const fbReview = props.feedbackSummary.reviews


  return (
    <BaseComponent
      style={style}
      className={`review-section x-utu-section-no-border-${
        isDark ? "dark" : "light"}`}
      excludeBootstrap
      excludeFonts
    >
      <FeedbackDetailsStatusView submitStatus={props.submitStatus}>
        <section className="review-card-section">
          {fbReview.length > 0 ? fbReview.map((rev:any) => (
            <div className={`review-card x-utu-section-no-border-mid-${envCondition}`}>
              <div className="review-card-group">
                {rev.image ? <img className="review-img" src={rev.image}
                  onError={e => {
                    (e.target as HTMLImageElement).onerror = null;
                    (e.target as HTMLImageElement).src = "";
                    (e.target as HTMLImageElement).className = "";
                    (e.target as HTMLImageElement).alt = "";
                  }} alt="person face" /> :
                  <div className="ml-5 video-avatar"><Avatar {...props} /></div>}
                <div className="review-text">
                  <div className={`review-text-sum review-text-${envCondition}`}>{rev.summaryText}</div>
                  <div className={`review-text-cont review-text-${envCondition}`}><i> {rev.content} </i> </div>
                </div>
              </div>
            </div>
          )) : 
            <div className={`review-card x-utu-section-no-border-mid-${envCondition}`}>
              <div className="review-card-group">
                
                <div className="ml-5 video-avatar rounded-circle"><Avatar {...props} /></div>
                <div className="review-text">
                  <div className={`review-text-sum review-text-${envCondition}`}>No reviews available</div>
                  <div className={`review-text-sum review-text-${envCondition}`} />
                  <div className={`review-text-cont review-text-${envCondition}`}>
                    when you have been reviewed by others, you will see their written reviews
                  </div>
                </div>
              </div>
            </div>
          }
        </section>
      </FeedbackDetailsStatusView>
    </BaseComponent>
  );
}
