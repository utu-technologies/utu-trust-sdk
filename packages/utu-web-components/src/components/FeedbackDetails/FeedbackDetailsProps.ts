import {IBaseOwnProps} from "../BaseComponent";
import {
  ATTR_BTN_COLOR,
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  TAG_FEEDBACK_DETAILS_POPUP
} from "../../names";
import {IFeedback, SubmitStatus} from "../../hooks/api";

export interface IFeedbackDetailsProps extends IBaseOwnProps {
  [ATTR_SOURCE_UUID]: string;
  [ATTR_TARGET_UUID]: string;
  [ATTR_BTN_COLOR]: string;
  [ATTR_THEME_COLOR]: string;
  [TAG_FEEDBACK_DETAILS_POPUP]: string;

  // for internal use:
  title: any;
  feedbackSummary: IFeedback | undefined;
  submitStatus: SubmitStatus
}
