/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import {BaseComponent, getBaseProps} from "../BaseComponent";
import { IFeedbackDetailsProps } from "./FeedbackDetailsProps";
import Logo from "../Images"
import style from "./FeedbackDetails.scss";
import { EndorsementsList } from "../EndorsementsList";
import { StarRatingShow } from "../StarRatingShow";
import BadgesShow from "../BadgesShow";
import { VideoShow } from "../VideoShow";
import { ReviewShow } from "../ReviewShow";
import {ATTR_SOURCE_UUID, ATTR_TARGET_UUID, ATTR_THEME_COLOR, TAG_FEEDBACK_DETAILS_POPUP} from "../../names";
import {useFeedbackSummaryApi} from "../../hooks/api";


export default function FeedbackDetails(props: IFeedbackDetailsProps) {

  // environments
  const [isDark, setIsDark] = useState(false);
  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_DETAILS_POPUP);
  const sourceUuid = props[ATTR_SOURCE_UUID];
  const targetUuid = props[ATTR_TARGET_UUID];
  const { feedbackSummary, submitStatus } = useFeedbackSummaryApi(apiUrl, sourceUuid, targetUuid);

  return (
    <BaseComponent style={style}
      className="utu-feedback-details">
      <div className="mobile-view">
        <VideoShow {...props} feedbackSummary={feedbackSummary}  submitStatus={submitStatus}/>
        <StarRatingShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        {/* <StarRatingShow {...props} /> */}
        <BadgesShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        <EndorsementsList {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        <ReviewShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        <section className="logo-section">
          <div className="logo-position"><Logo /></div>
        </section>
      </div>

      <div className="desktop-view">
        <div className={`desktop-view-block-1 desktop-view-block-1-${
          isDark ? "dark" : "light"
        }`
        }>
          <div className="desktop-view-video-stars">
            <VideoShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
            <StarRatingShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
            {/* <StarRatingShow {...props}/> */}

          </div>

          <ReviewShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        </div>
        <div className={`desktop-view-block-2 desktop-view-block-2-${
          isDark ? "dark" : "light"
        }`
        }>
          <BadgesShow {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
          <EndorsementsList {...props} feedbackSummary={feedbackSummary} submitStatus={submitStatus}/>
        </div>
        <section className="logo-section">
          <div className="logo-position-details"><Logo /></div>
        </section>
      </div>

    </BaseComponent>
  );
}
