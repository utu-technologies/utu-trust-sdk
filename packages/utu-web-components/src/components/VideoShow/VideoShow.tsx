/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import { BaseComponent } from "../BaseComponent";
import {
  ATTR_THEME_COLOR
} from "../../names";
import Player from "../RecordVideo/partials/Player";
import PopUp from "../PopUp/PopUp";
import Avatar from "../FeedbackDetails/Avatar";
import Logo from "../Images"
import { IFeedbackDetailsProps } from "../FeedbackDetails/FeedbackDetailsProps";
import style from "./VideoShow.scss";
import FeedbackDetailsStatusView from "../SubmitStatusView/FeedbackDetailsStatusView";

export default function VideoShow(props: IFeedbackDetailsProps) {
  // environments
  const [isDark, setIsDark] = useState(false);

  // environment conditionals
  const envCondition =
   isDark ? "dark" : "light"

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  if (!props.feedbackSummary) {
    return (<FeedbackDetailsStatusView submitStatus={props.submitStatus}><div /></FeedbackDetailsStatusView>);
  }

  if (!props.feedbackSummary.videos) {
    return  (<FeedbackDetailsStatusView submitStatus={3}><div /></FeedbackDetailsStatusView>);
  }

  const { videos } = props.feedbackSummary.videos;
  const profileImage = videos instanceof Array ? videos.map(img => img.image) :  
    <div className={`ml-5 no-data-avatar avatar-no-img-${envCondition}`}><Avatar {...props} /></div>;
  const profileUrl = videos instanceof Array ? videos.map(img => img.url) : "";
  console.log("profileUrl", profileUrl);
  const profileText = props.feedbackSummary.videos.summaryText;

  // set of profile images 
  const videoImg = videos instanceof Array ? videos.map(img => (
    <li className="avatar-img-top-list-item" key={img.image}>
      <img
        className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
        src={img.image}
        alt="profile"
      />
    </li>
  ) ) : "";

  // component for everything inclusing profile images, video or undefined
  const UserImageVideo = ({ url, image }: any) => {
    console.log("UserImageVideo", url);
    const [videoVisible, setVideoVisible] = useState(false);
  
    return (
      <div>
        <div  className="avatar" onClick={() => { setVideoVisible(!videoVisible) }}>
          {!image || image.length === 0 ?
            <div style={{width: "2rem"}} className={`avatar-no-img avatar-no-img-${envCondition}`}><Avatar {...props} /></div>
            :     <li className="avatar-img-top-list-item" key={image}> <img
              className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
              src={image}
              alt="profile"
            /> </li>
          }
        </div>
        {videoVisible && url &&
          <PopUp closeButtonOverlay onClose={() => setVideoVisible(false)}>
            <BaseComponent style={style}
              className="utu-feedback utu-feedback-video-popup">
              {
                Player(url, false)
              }
              <div className="logo-position-player">
                <Logo />
              </div>
            </BaseComponent>
          </PopUp>
        }
      </div>
    )
  }

  return (
    <BaseComponent
      style={style}
      className={`video-section x-utu-section-no-border-${
        isDark ? "dark" : "light"
      }`
      }
      excludeBootstrap
      excludeFonts
    >
      <FeedbackDetailsStatusView submitStatus={props.submitStatus}>
        <section
          className={`avatar-img-top-video 
          ${profileUrl ? `x-utu-section-${envCondition}` : ""}`}>
          <div className="avatar-img-top-video-container">
            {videos.length === 0 ? 
              <div className={`ml-5 no-data-avatar rounded-circle avatar-no-img-${envCondition}`}><Avatar {...props} /></div> 
              :               
              <div style={{display: "flex"}} className="avatar-img-top-video-profile">
                {profileImage ? videos.map(i => (<UserImageVideo url={i.url} image={i.image}/>) ) :
                  <img
                    className={`avatar-img-top-details rounded-circle summary-image-item summary-image-item-${envCondition}`}
                    src="https://utu.io/wp-content/uploads/2021/07/P1.png"
                    alt="profile"
                  />
                }
              </div>
            }
            <div className="avatar-img-top-video-text-container"> 
              <p className="avatar-img-top-video-text">{profileText}</p>
            </div>
           
          </div>
        </section>
      </FeedbackDetailsStatusView>
    </BaseComponent>
  );
}


// <UserImageVideo url={profileUrl} image={profileImage}/>