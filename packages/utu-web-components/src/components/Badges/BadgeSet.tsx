/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useContext, useState } from "preact/hooks";
import { BaseComponent, getBaseProps } from "../BaseComponent";
import { useFeedbackApi } from "../../hooks/api";
import style from "./Badges.scss";
import {
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID, ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID,
  TAG_FEEDBACK_FORM
} from "../../names";
import {IFeedbackProps} from "../FeedbackForm/FeedbackProps";
import SubmitStatusView from "../SubmitStatusView/SubmitStatusView";
import { WalletAddressContext } from "../WalletConnect/WalletAddressContext";


export interface BadgeSetProps extends IFeedbackProps {
  id: string;
  badges: Array<any>;
  image: string;
}

// export default function BadgeSet({ id, badges, ...props } : BadgeSetProps) {
export default function BadgeSet({ id, badges, image, ...props } : BadgeSetProps) {
  const [selectedQualifier, setSelectedQualifier] = useState("");

  console.log("BadgeSet", badges);
  // environments
  const [isDark, setIsDark] = useState(false);


  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  const walletAddress = useContext(WalletAddressContext);
  if (props[ATTR_SOURCE_UUID] === "address") {
    // eslint-disable-next-line no-param-reassign
    props[ATTR_SOURCE_UUID] = walletAddress;
  }

  const isSelected = (qualifier: string) => selectedQualifier === qualifier;

  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_FORM);

  const { sendFeedback, submitStatus } = useFeedbackApi(
    apiUrl,
    props[ATTR_SOURCE_UUID],
    props[ATTR_TARGET_UUID],
    props[ATTR_TRANSACTION_ID]
  );

  return (
    <BaseComponent style={style}>
      <section className="x-utu-badge-set-section">
        <div className="x-utu-badge-img-big-section">
          <img src={image} className="x-utu-badge-img-big" alt="badges" /></div>
        <div key={id} className="x-utu-badge-set-container">
          <h3 className={`text-left x-utu-badge-text-h3 x-utu-badge-text-h3-${
            isDark ? "dark" : "light"      
          }`}>{id}</h3>
          <SubmitStatusView submitStatus={submitStatus}>
            <div className="x-utu-badge-img-container">
              {badges  === undefined ? <div /> : badges.map(({ qualifier, badge }: any) => (
                <button
                  className={ `x-utu-btn-icon ${ isSelected(qualifier) ? "x-utu-badge-img" : "x-utu-badge-disable" }` }
                  type="button"
                  onClick={() => {
                    const qualifierToSet = isSelected(qualifier) ? "neutral" : qualifier;
                    sendFeedback({ badges: { [id]: qualifierToSet } });
                    setSelectedQualifier(qualifierToSet);
                  }}
                >
                  <div className="x-utu-badge-item-text">
                    <img src={badge.image} className="x-utu-badge-img" alt="badges" />
                  </div>
                </button>
              ))}
            </div>
          </SubmitStatusView>
        </div>
      </section>

    </BaseComponent>
  );
}
