/* eslint-disable no-nested-ternary */
import { h } from "preact";
import { useState } from "preact/hooks";
import { useBadgesApi } from "../../hooks/api";
import { BaseComponent, getBaseProps } from "../BaseComponent";
import BadgeSet from "./BadgeSet";
import { IFeedbackProps } from "../FeedbackForm/FeedbackProps";
import {ATTR_THEME_COLOR, TAG_FEEDBACK_FORM} from "../../names";

export default function Badges(props: IFeedbackProps) {
  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_FORM);
  const { badgeSets } = useBadgesApi(apiUrl);
  console.log(badgeSets);

  // environments

  const [isDark, setIsDark] = useState(false);

  if (props[ATTR_THEME_COLOR] === "dark") {
    setIsDark(true);
  }

  return (
    <BaseComponent
      className={`x-utu-section-no-border-${
        isDark ? "dark" : "light"
      }`
      }
      excludeBootstrap
      excludeFonts
    >
      <div>
        <h3 className="x-utu-badge-section-title px-5">Award Badges</h3>
        <div className="x-utu-badge-section">
          {badgeSets.map(({ id, badges, image }) => (
            <BadgeSet {...props} id={id} image={image} badges={badges} />
          ))}
        </div>
      </div>
      
    </BaseComponent>
  );
}
