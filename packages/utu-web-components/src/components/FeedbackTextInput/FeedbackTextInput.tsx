/* eslint-disable no-nested-ternary */
import { h } from "preact";
import {useContext} from "preact/hooks";
import { useForm } from "react-hook-form";
import { BaseComponent, getBaseProps } from "../BaseComponent";
import { IFeedbackData, useFeedbackApi } from "../../hooks/api";
import {WalletAddressContext} from "../WalletConnect/WalletAddressContext";
import {
  ATTR_BTN_COLOR,
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID,
  ATTR_THEME_COLOR,
  ATTR_TRANSACTION_ID,
  TAG_FEEDBACK_FORM
} from "../../names";
import {IFeedbackProps} from "../FeedbackForm/FeedbackProps";
import style from "./FeedbackTextInput.scss";
import SubmitStatusView from "../SubmitStatusView/SubmitStatusView";

const minLength = 5;

export default function FeedbackTextInput(props: IFeedbackProps) {
  const envCondition = props[ATTR_THEME_COLOR] ?? "light";

  const walletAddress = useContext(WalletAddressContext);
  if (props[ATTR_SOURCE_UUID] === "address") {
    props[ATTR_SOURCE_UUID] = walletAddress;
  }

  const { apiUrl } = getBaseProps(props, TAG_FEEDBACK_FORM);
  const { sendFeedback, submitStatus } = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID],
    props[ATTR_TRANSACTION_ID]);
  console.log("FeedbackTextInput", submitStatus);

  const { register, errors, handleSubmit } = useForm();

  function onSubmit(feedback: IFeedbackData) {
    sendFeedback(feedback);
  }

  return <BaseComponent
    style={style}
    className={`x-utu-feedback-text-input-section x-utu-section x-utu-section-no-border-${envCondition}`}
    excludeBootstrap excludeFonts>
    <form onSubmit={handleSubmit(onSubmit) as () => void} className="x-utu-feedback-text-input-form">
      <h3 className="x-utu-feedback-text-space">Text Review</h3>
      <textarea
        className={`x-utu-feedback-text-area x-utu-feedback-text-area-${envCondition}`}
        ref={register({ required: true, minLength })} name="review" placeholder="Your text review" />
      {
        errors.review && <div className="mt-1 error">* Feedback should be at least {minLength} characters long</div>
      }
      <SubmitStatusView submitStatus={submitStatus}>
        <button
          style={{backgroundColor: props[ATTR_BTN_COLOR] === undefined ? null : `${props[ATTR_BTN_COLOR]}`}}
          className={`x-utu-feedback-text-input-btn x-utu-btn x-utu-btn-${envCondition}`} type="submit">Submit text review</button>
      </SubmitStatusView>
    </form>
  </BaseComponent>;
}
