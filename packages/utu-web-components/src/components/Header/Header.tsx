import { h } from "preact";
import {BaseComponent} from "../BaseComponent";
import style from "./Header.scss";


export default function Header() {
  return (
    <BaseComponent style={style}>
      <section className="x-utu-header x-utu-section">
        <h3> Feedback From Your Network </h3>
      </section>
  
    </BaseComponent>
  )
}
