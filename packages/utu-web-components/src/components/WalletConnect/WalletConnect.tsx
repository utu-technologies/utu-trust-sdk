import { h } from "preact";
import { providers } from "ethers";
import style from "./WalletConnect.scss";
import {BaseComponent} from "../BaseComponent";
import {wallet} from "./WalletAddressContext"
import { useWalletConnectApi } from "../../hooks/api";

export default function WalletConnect() {
  const {web3Modal} = useWalletConnectApi();


  async function connectWallet() {
    const provider = await web3Modal.connect();
    const ethersProvider = new providers.Web3Provider(provider);
    const userAddress = await ethersProvider.getSigner().getAddress();
    wallet.address = userAddress;
  }

  return (
    <BaseComponent style={style} className="x-utu-wallet-connect">
      <section className="x-utu-wallet-connect-section">
        <p className="x-utu-wallet-connect-text">
          See UTU trust signals from your network
        </p>
        <div>
          <button
            onClick={connectWallet}
            className="x-utu-wallet-text-input-btn x-utu-btn" type="submit">
            Connect Wallet
          </button>
        </div>
      </section>
    </BaseComponent>);
}