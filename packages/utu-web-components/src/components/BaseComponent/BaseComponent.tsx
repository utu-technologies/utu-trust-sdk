import { h, ComponentChildren, Ref, Fragment } from "preact";
import {wallet, WalletAddressContext} from "../WalletConnect/WalletAddressContext";
import {
  ATTR_API_URL,
  EVENT_UTU_CONFIG,
  UTU_API_BASE_URL_TEST,
  UTU_API_BASE_URL_PRODUCTION,
  ATTR_BTN_COLOR,
  ATTR_THEME_COLOR,
  TAG_FEEDBACK_DETAILS_POPUP,
  UTU_SOCIAL_LINK_URL_PRODUCTION,
  UTU_SOCIAL_LINK_URL_TEST
} from "../../names";

export interface IBaseOwnProps {
  [ATTR_API_URL]?: string;
  [ATTR_BTN_COLOR]: string;
  [ATTR_THEME_COLOR]: string;
  [TAG_FEEDBACK_DETAILS_POPUP]:string;
}

export interface IOwnProps {
  children?: ComponentChildren;
  forwardedRef?: Ref<HTMLDivElement>;
  className?: string;
  style?: {
    [className: string]: string;
  };
  excludeFonts?: boolean;
  excludeBootstrap?: boolean;
}

export interface IConfig {
  /**
   * Determines whether production mode shall be used. If true, connects to the UTU production API and Polygon
   * mainnet by default; otherwise default is to connect to testing services and Mumbai testnet.
   */
  production: boolean,

  /**
   * Limits the number of endorsements to show in the feedback details screen.
   */
  feedbackDetailsMaxEndorsements: number
}

export const config = {
  production: false,
  feedbackDetailsMaxEndorsements: 5
};

window.addEventListener(EVENT_UTU_CONFIG, (event: CustomEvent<IConfig>) => {
  console.log("UTU SDK received config:", JSON.stringify(event.detail));
  Object.assign(config, event.detail);
});

export function getBaseProps(
  props: IBaseOwnProps,
  componentName: string
) {
  const apiUrlDefault = config.production ? UTU_API_BASE_URL_PRODUCTION : UTU_API_BASE_URL_TEST;
  const apiUrl = props[ATTR_API_URL] || process.env.UTU_APP_BASE_URL || apiUrlDefault;

  if (!apiUrl) {
    throw new Error(`${ATTR_API_URL} attribute of ${componentName} was not set`);
  }

  return { apiUrl };
}
export function getUtuSocialLink(){
  const socialLinkUrl = config.production ? UTU_SOCIAL_LINK_URL_PRODUCTION : UTU_SOCIAL_LINK_URL_TEST;
  return { socialLinkUrl };
}

export function BaseComponent({
  children,
  forwardedRef,
  className,
  style,
  excludeBootstrap,
  excludeFonts
}: IOwnProps) {
  return (
    <WalletAddressContext.Provider value={wallet.address}>
      <div ref={forwardedRef} className={className}>
        <style>
          {!excludeFonts && (
            <Fragment>
            @import
            "https://fonts.googleapis.com/css2?family=Roboto:wght@300;500&display=swap";
            </Fragment>
          )}
          {!excludeBootstrap && (
            <Fragment>
            @import
            "https://getbootstrap.com/docs/5.0/dist/css/bootstrap-reboot.min.css";
            @import
            "https://getbootstrap.com/docs/5.0/dist/css/bootstrap-grid.min.css";
            @import
            "https://getbootstrap.com/docs/5.0/dist/css/bootstrap-utilities.min.css";
            </Fragment>
          )}
          {style}
        </style>
        {children}
      </div>
    </WalletAddressContext.Provider>
  );
}
