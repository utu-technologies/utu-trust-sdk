import register from "preact-custom-element";

import {
  Recommendation,
  Root,
  FeedbackFormPopup,
  Header,
  WalletConnect,
  UtuAppSocialLink,
  UttBalance,
  DisconnectWallet
} from "./components";
import { FeedbackForm } from "./components/FeedbackForm";
import FeedbackPopup from "./components/FeedbackPopup";
import FeedbackDetails from "./components/FeedbackDetails";

import {
  TAG_ROOT,
  TAG_RECOMMENDATION,
  ATTR_TARGET_UUIDS,
  TAG_FEEDBACK_FORM,
  TAG_FEEDBACK_FORM_POPUP,
  ATTR_API_URL,
  ATTR_BTN_COLOR,
  ATTR_SOURCE_UUID,
  ATTR_TARGET_UUID,
  ATTR_TARGET_TYPE,
  EVENT_UTU_IDENTITY_DATA_READY,
  LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA,
  TAG_FEEDBACK_DETAILS_POPUP,
  TAG_FEEDBACK_DETAILS,
  ATTR_TRANSACTION_ID,
  ATTR_ENDORSEMENT_NETWORK,
  TAG_HEADER,
  TAG_WALLET_CONNECT,
  TAG_UTU_APP_LINK,
  TAG_UTT_BALANCE,
  TAG_WALLET_DISCONNECT
} from "./names";

export { addressSignatureVerification } from "./hooks/auth";

window.addEventListener(EVENT_UTU_IDENTITY_DATA_READY, (event: CustomEvent) => {
  localStorage.setItem(LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA, JSON.stringify(event.detail));
});


register( Header, TAG_HEADER, [], { shadow: true } );

register( UtuAppSocialLink, TAG_UTU_APP_LINK, [], { shadow: true } );

register( WalletConnect, TAG_WALLET_CONNECT, [], {shadow: true});

register(DisconnectWallet, TAG_WALLET_DISCONNECT, [], { shadow: true });

register(UttBalance, TAG_UTT_BALANCE, [], { shadow: true });

register(Root, TAG_ROOT, [ ATTR_API_URL, ATTR_SOURCE_UUID, ATTR_TARGET_TYPE,
  ATTR_TARGET_UUIDS], { shadow: true });

register(Recommendation, TAG_RECOMMENDATION, [ATTR_TARGET_UUID], {
  shadow: true
});

register(
  FeedbackForm,
  TAG_FEEDBACK_FORM,
  [
    ATTR_API_URL,
    ATTR_SOURCE_UUID,
    ATTR_TARGET_UUID,
    ATTR_TRANSACTION_ID,
    ATTR_ENDORSEMENT_NETWORK
  ],
  {
    shadow: true
  }
);

register(
  FeedbackFormPopup,
  TAG_FEEDBACK_FORM_POPUP,
  [
    ATTR_API_URL,
    ATTR_SOURCE_UUID,
    ATTR_TARGET_UUID,
    ATTR_TRANSACTION_ID,
    ATTR_ENDORSEMENT_NETWORK,
    ATTR_BTN_COLOR
  ],
  {
    shadow: true
  }
);

register(
  FeedbackPopup,
  TAG_FEEDBACK_DETAILS_POPUP,
  [
    ATTR_API_URL,
    ATTR_BTN_COLOR
  ],
  {
    shadow: true
  });

register(
  FeedbackDetails,
  TAG_FEEDBACK_DETAILS,
  [
    ATTR_API_URL,
    ATTR_SOURCE_UUID,
    ATTR_TARGET_UUID
  ],
  {
    shadow: true
  });
