# UTU Trust SDK Changelog

## v1.0.1 (20 April 2021)
* Initial release supporting feedbac summary, badges, star rating and text 
  review
  
## v1.1.0 (7 September 2021)
* Added video reviews as feedback component
* Various design improvements
* Cleaned up repository to include README, CHANGELOG and LICENSE in the 
  utu-web-components sub-package of the utu-trust-sdk workspace 

## v1.1.1 (7 September 2021)
* Include README.md, CHANGELOG.md and LICENSE.TXT

## v1.1.2 (7 September 2021)
* Re-publish as production build

## v1.1.3 (7 September 2021)
* Remove explicit including of README.md, CHANGELOG.md and LICENSE.TXT 
  because this happens automatically


