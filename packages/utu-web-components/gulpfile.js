const gulp = require("gulp");
const del = require("del");
const rollup = require("rollup");
const { babel } = require("@rollup/plugin-babel");
const resolve = require("@rollup/plugin-node-resolve").default;
const commonjs = require("@rollup/plugin-commonjs");
const replace = require("@rollup/plugin-replace");
const alias = require("@rollup/plugin-alias");
const postcss = require("rollup-plugin-postcss");
const fileSize = require("rollup-plugin-filesize");
const { terser } = require("rollup-plugin-terser");
const browserSync = require("browser-sync").create();
const dotenv = require("dotenv");
const sourcemaps = require("rollup-plugin-sourcemaps");
const strip = require("@rollup/plugin-strip");
const json = require("@rollup/plugin-json");
const pkg = require("./package.json");

dotenv.config();

const jsExtensions = [".js", ".jsx", ".ts", ".tsx"];

const IS_PRODUCTION = process.env.NODE_ENV === "production";

gulp.task("compile:umd", async () => {
  const bundle = await createBundle();

  return await bundle.write({
    file: pkg.main,
    format: "umd",
    name: "utuSdk",
    sourcemap: !IS_PRODUCTION
  });
});

gulp.task("compile:es", async () => {
  const bundle = await createBundle();

  return await bundle.write({
    file: pkg.module,
    format: "es",
    name: "utuSdk",
    sourcemap: !IS_PRODUCTION
  });
});

// gulp.task('compile', gulp.parallel('compile:umd', 'compile:es'));
gulp.task("compile", gulp.parallel("compile:umd"));


gulp.task("clean", () => {
  return del(["dist/**"])
});

gulp.task("copy-html", () => {
  return gulp.src(["src/*.html"]).pipe(gulp.dest("dist"));
});

gulp.task("serve", () => {
  return browserSync.init({
    server: {
      baseDir: "dist",
      port: 3050
    }
  })
});

gulp.task("reload", cb => {
  browserSync.reload();
  cb();
});

gulp.task("build", gulp.series(
  "clean",
  "compile",
  !IS_PRODUCTION ? "copy-html" : cb => cb()
));

gulp.task("watch", () => {
  gulp.watch("src/*.html", gulp.series("copy-html", "reload"));
  gulp.watch(["src/**/*.ts", "src/**/*.tsx", "src/**/*.scss"], gulp.series("build", "reload"));
});

gulp.task("default", gulp.series("build", gulp.parallel("serve", "watch")));

function createBundle() {
  return rollup.rollup({
    input: "src/index.ts",
    plugins: [
      json(),
      strip({
        functions: [ "console.log"]
      }),
      sourcemaps(),
      replace({
        values: {
          "process.env.NODE_ENV": JSON.stringify("development")
        },
        preventAssignment: true
      }),
      alias({
        entries: [
          { find: "react", replacement: "preact/compat" },
          { find: "react-dom", replacement: "preact/compat" }
        ]
      }),
      postcss(),
      babel({
        extensions: jsExtensions,
        babelHelpers: "bundled",
        inputSourceMap: !IS_PRODUCTION,
        sourceMaps: !IS_PRODUCTION
      }),
      resolve({
        extensions: jsExtensions,
        browser: true,
        preferBuiltins: true
      }),
      commonjs(),
      IS_PRODUCTION && terser(),
      fileSize()
    ]
  });
}
