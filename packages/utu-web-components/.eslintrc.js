module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },
  settings: {
    react: {
      version: "17.0.0"
    }
  },
  extends: [
    // "airbnb-typescript",
    "airbnb-typescript-prettier"
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    "react/react-in-jsx-scope": "off",
    "react/no-unescaped-entities": 0,
    "react/destructuring-assignment": 0,
    "import/prefer-default-export": "off",
    "@typescript-eslint/comma-dangle": ["error", "never"],
    "indent": "off",
    "@typescript-eslint/indent": ["error", 2],
    "@typescript-eslint/quotes": [2, "double"],
    "max-len": ["error", { "code": 160 }],
    "arrow-parens": ["error", "as-needed"],
    "prettier/prettier": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/no-use-before-define": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "react/jsx-props-no-spreading": "off"
  }
};
