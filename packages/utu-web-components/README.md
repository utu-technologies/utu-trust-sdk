# UTU Trust SDK

    
UTU’s Trust API seamlessly serves up personalized recommendations for trusted service providers on sharing platforms to drive your conversion, satisfaction, retention, and viral acquisition.

[More information about UTU](https://utu.io)

UTU Trust SDK provides web-components that access UTU’s Trust API and allows you simple integration with its services.

Find the [repository here](https://bitbucket.org/utu-technologies/utu-trust-sdk)

## Features

- Integration with UTU's Trust API
- Compatible with React, Angular, Vue and other popular UI libs and frameworks
- Shadow DOM
- Customized styles, which are not interfere with global CSS rules on a page

## Install

```bash
$ npm install @ututrust/web-components
```

## Configuration options 

The SDK can be configured by dispatching an `utuConfig` event. It 
currently supports only one element: `production` (`boolean`).

If set to `true`, the SDK will connect to UTU's production API services. 
Otherwise (default), it will connect to UTU API testing services.

Use it like so, after the SDK has been loaded:
```
window.dispatchEvent(new CustomEvent("utuConfig", { detail: { production: true } }));
```

## Examples

You can find [some working examples in the repository](https://bitbucket.org/utu-technologies/utu-trust-sdk/src/dev/packages/examples.)

Before running examples, run ```npm install``` in the root repository folder, then go to ```/packages/utu-web-components``` and execute ```npm run build``` command. After that local copy of ```@ututrust/web-components``` will be available in all examples.

It is advised to review ```Quickstart Vanilla JS``` first before checking another samples.

### Quickstart Vanilla JS

Place ```<x-utu-root>``` custom tag. It is a parent component for recommendations:

```html
<script src="../../utu-web-components/dist/index.js"></script>

<!-- Once UTU SDK is included on your page, -->
<!-- it registers custom tags: <x-utu-root> and <x-utu-recommendation> -->

<!-- <x-utu-root> handles recommendations loading from API. -->

<x-utu-root api-key="[place your utu api key here]">
  <ul></ul>
</x-utu-root>
```

The next step is defining recommendations ```<x-utu-recommendation>```:

```html
<x-utu-root api-key="[place your utu api key here]">
  <ul>
    <li>
      <x-utu-recommendation recommendation-id="e541df40-74b6-478e-a7da-7a9e52778700" />
    </li>
  </ul>
</x-utu-root>
```

### Quickstart React

Just import ```@ututrust/web-components``` and you are ready to start using UTU SKD.

```jsx
import '@ututrust/web-components';

function App() {

  const offerIds = [
    'e541df40-74b6-478e-a7da-7a9e52778700'
  ];

  return <div className="App">
      <x-utu-root api-key="<place your utu api key here>">
        <ul>
          {
            offerIds.map(offerId =>
              <li key={offerId}>
                <x-utu-recommendation target-uuid={offerId} />
              </li>
            )
          }
        </ul>
      </x-utu-root>
    </div>;
}

export default App;
```

### Quickstart Vue
We need to tell Vue, that custom components are going to be used. This requires some WebPack configuration:

```js
// vue.config.js
module.exports = {
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        return {
          ...options,
          compilerOptions: {
            ...(options.compilerOptions || {}),
            isCustomElement: tag => tag.startsWith('x-')
          }
        }
      })
  }
}
```

Once you import ```@ututrust/web-components```, ```<x-utu-root>``` and ```<x-utu-recommendation>``` are ready to use.

```html
<template>
  <div class="offers">
    <x-utu-root ref="utu-root" api-key="[place your utu api key here]">
      <ul>
        <li v-for="offerId in offerIds" :key="offerId">
          <x-utu-recommendation :target-uuid="offerId" />
        </li>
      </ul>
    </x-utu-root>
  </div>
</template>

<script>
import "@ututrust/web-components";

export default {
  name: "Recommendations",
  data: function () {
    return {
      offerIds: ["e541df40-74b6-478e-a7da-7a9e52778700"]
    };
  },
};
</script>
```

### Quickstart Angular

Add ```CUSTOM_ELEMENTS_SCHEMA``` to module schemas

```js
// app.module.ts
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
```

Import ```@ututrust/web-components```. We assume offer ids are in 
```offerIds``` to your component.

```js
// app.module.ts
import { Component } from '@angular/core';
import '@ututrust/web-components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  offerIds = ['e541df40-74b6-478e-a7da-7a9e52778700']
}
```

Add template:
```html
<!-- app.component.html -->
<div class="content" role="main">
  <x-utu-root api-key="[place your utu api key here]">
    <ul>
      <li *ngFor="let offerId of offerIds">
        <x-utu-recommendation [target-uuid]="offerId"></x-utu-recommendation>
      </li>
    </ul>
  </x-utu-root>
</div>
```

## Mocking API Response

To mock an UTU API response, specify ```api-url``` of ```<x-utu-root>```:

```html
  <x-utu-root api-key="[place your utu api key here]" api-url="/api-mocks/ranking.json">
    <ul id="list"></ul>
  </x-utu-root>
```

Example of a JSON file can be find at ```/packages/utu-web-components/src/api-mock```

## Local Development
Development stack:
- [preactjs](https://preactjs.com)
- [web components](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
- [rollupjs](https://rollupjs.org/guide/en)
- [babel](https://babeljs.io)
- [gulp](https://gulpjs.com)
- [jest](https://jestjs.io)
- [typescript](https://www.typescriptlang.org)
- [scss](https://sass-lang.com)

```preactjs``` allows developing [React](https://reactjs.org)-like components with state management, hooks and JSX, however its size is about 4kb only, which is create for lib development.

To convert React-like component into web component, we use [this](https://preactjs.com/guide/v10/web-components) approach.

Follow steps below to start local development of UTU Trust SDK.

Install dependencies

```bash
$ npm install
```

Start watch mode with preview in a browser

```bash
$ npm start
```

Production build

```bash
$ npm run build
```

Run tests in watch mode

```bash
$ npm run test
```

Check lint errors and fix them if possible

```bash
$ npm run lint
```

Create tar npm package, so it can be later installed as npm dependency. 

```bash
$ npm pack
```

This will generate a ```.tgz``` file at the directory’s root with a structure like this: ```{name}-{version}.tgz```. You can install this package with command like this:

```bash
$ npm install <path to the package>/utu-web-components-1.0.0.tgz
```
