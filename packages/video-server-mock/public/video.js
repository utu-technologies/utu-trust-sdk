const CHUNK_SIZE = 250 * 1024;

const recordBtn = document.querySelector("#record-btn");
const alertBox = document.querySelector("#support-alert");
const progressBox = document.querySelector("#progress-note");
const uploadList = document.querySelector("#upload-list");

let stopRecording = null;
let upload = null;

if (!tus.isSupported) {
  alertBox.classList.remove("hidden");
}

if (!recordBtn) {
  throw new Error("Record button not found on this page. Aborting upload-demo.");
}

recordBtn.addEventListener("click", (e) => {
  e.preventDefault()
  if (stopRecording) {
    recordBtn.textContent = "Start Recording";
    stopRecording();
  } else {
    recordBtn.textContent = "Stop Recording";
    startStreamUpload();
  }
})

function startUpload(file) {
  const endpoint = "http://localhost:2080/uploads";
  const options = {
    resume: false,
    endpoint,
    chunkSize: CHUNK_SIZE,
    retryDelays: [0, 1000, 3000, 5000],
    uploadLengthDeferred: true,
    metadata: {
      filename: "webcam.webm",
      filetype: "video/webm",
    },
    onError(error) {
      if (error.originalRequest) {
        if (window.confirm(`Failed because: ${error}\nDo you want to retry?`)) {
          upload.start();
          return;
        }
      } else {
        window.alert(`Failed because: ${error}`);
      }

      reset()
    },
    onProgress(bytesUploaded) {
      progressBox.textContent = `Uploaded ${bytesUploaded} bytes so far.`;
    },
    onSuccess() {
      const listItem = document.createElement("li");
      listItem.className = "list-group-item";

      const video = document.createElement("video");
      video.controls = true;
      video.src = upload.url;
      listItem.appendChild(video);

      const lineBreak = document.createElement("br");
      listItem.appendChild(lineBreak);

      const anchor = document.createElement("a");
      anchor.textContent = `Download ${options.metadata.filename}`;
      anchor.href = upload.url;
      anchor.className = "btn btn-success mt-2";
      listItem.appendChild(anchor);

      uploadList.appendChild(listItem);
      reset();
    },
  }

  upload = new tus.Upload(file, options);
  upload.start();
}

function startStreamUpload() {
  const options = {
    video: {
      height: {
        ideal: 720
      },
      width: {
        ideal: 1280
      },
    },
    audio: true
  };

  navigator.mediaDevices.getUserMedia(options)
    .then(stream => {
      const options = {
        audioBitsPerSecond: 128000,
        videoBitsPerSecond: 6500000,
        mimeType: 'video/webm'
      };

      const mr = new MediaRecorder(stream, options);
      const chunks = [];
      let done = false;
      let onDataAvailable = null;

      mr.onerror = onError;
      mr.onstop = () => {
        done = true;
        if (onDataAvailable) onDataAvailable(readableRecorder.read());
      }
      mr.ondataavailable = event => {
        chunks.push(event.data);
        if (onDataAvailable) {
          onDataAvailable(readableRecorder.read());
          onDataAvailable = undefined;
        }
      }

      mr.start(1000);

      const readableRecorder = {
        read() {
          if (done && chunks.length === 0) {
            return Promise.resolve({ done: true });
          }

          if (chunks.length > 0) {
            return Promise.resolve({ value: chunks.shift(), done: false });
          }

          return new Promise((resolve) => { onDataAvailable = resolve });
        },
      }

      startUpload(readableRecorder)

      stopRecording = () => {
        stream.getTracks().forEach(t => t.stop());
        mr.stop();
        stopRecording = null;
      }
    })
    .catch(onError);
}

function reset() {
  upload = null;
}

function onError(error) {
  console.log(error);
  alert(`An error occurred: ${error}`);

  upload = null;
  stopRecording = null;
  recordBtn.textContent = "Start Recording";
}
